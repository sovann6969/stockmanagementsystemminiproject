package models;

import core.IProduct;
import core.IProductFile;
import util.Resource;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Product extends IProductFile implements IProduct {
    private String id;
    private String name;
    private String unitPrice;
    private String importedDate;
    private String qty;
    public String getQty() {
        return qty;
    }

    public Product(String id, String name, String unitPrice, String qty, String importedDate) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.importedDate = importedDate;
        this.qty = qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    private List<Product> lst = new ArrayList<Product>();
    public Product() { }

    public Product(String id, String name, String unitPrice, String importedDate) {
        this.id = id;
        this.name = name;
        this.unitPrice = unitPrice;
        this.importedDate = importedDate;
    }

    @Override
    public List<Product> get() {

        return new Product().read("text.txt");
    }

    @Override
    public Product get(int id) {
        String st = "text.txt";
        lst = new Product().read(st);
        Product p = new Product(lst.get(id).getId(),lst.get(id).getName(),lst.get(id).getUnitPrice(),lst.get(id).getImportedDate());
        return p;
    }

    @Override
    public Product insert(Product product) {
        return null;
    }

    @Override
    public Product delete(int id) {
        return null;
    }

    @Override
    public List<Product> search(int name) {
        return null;
    }

    @Override
    public List<Product> gotoPage(int page) {
        return null;
    }

    @Override
    public List<Product> first() {
        return null;
    }

    @Override
    public List<Product> last() {
        return null;
    }

    @Override
    public List<Product> previous() {
        return null;
    }

    @Override
    public List<Product> next() {
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    @Override
    public void write10M() {
        long lStartTime = System.currentTimeMillis();
        try {

            FileOutputStream fos = new FileOutputStream("src/files/text.txt");
            OutputStreamWriter w = new OutputStreamWriter(fos, "UTF-8");
            BufferedWriter bw = new BufferedWriter(w);

            for (int i = 1; i <= Resource.AMOUNT; i++) {
                String STR =  i + ",Coca Cola,"+ "2.0" +",12,"+"2019/31/11\n";
                bw.write(STR);
            }
            long lEndTime = System.currentTimeMillis();
            long output = lEndTime - lStartTime;
            System.out.println("Time Spend : " + output);
            bw.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(List<Product> products) {

    }

    @Override
    public void write(Product product) {

    }

    @Override
    public List<Product> read(String fileName) {
           String st = "src/files/" + fileName;
        ArrayList<Product> lst = new ArrayList<>();
        //ensure ... ini capacity
        lst.ensureCapacity(20_000_000);
        long lStartTime = System.currentTimeMillis();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(st), StandardCharsets.UTF_8)) {

            for (String line = null; (line = br.readLine()) != null;) {
                String [] str = line.split(",");
                Product p = new Product(str[0],str[1],str[2],str[3],str[4]);
                lst.add(p);
            }
            long lEndTime = System.currentTimeMillis();
            long output = lEndTime - lStartTime;
            System.out.println("Time Spend : " + output);
        } catch (IOException e) {
            e.printStackTrace();
        }



        return lst;
    }

    @Override
    public void save(List<Product> products) {

    }

    @Override
    public void backup() {

    }

    @Override
    public void restore() {

    }
}
