package core;

import models.Product;

import java.util.List;

public interface IDisplayTabularData {
    /**
     * Display all products in the table
     * include Pagination(Next, Previous, First, Last)
     * @param products List of products
     */
    void display(List<Product> products);


    void display(Product product);
    void setDisplayRowNumber(int n);

    void first();
    void last();
    void previous();
    void next();
}
