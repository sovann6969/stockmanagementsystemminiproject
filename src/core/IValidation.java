package core;

public interface IValidation {
    boolean isShortcut(String s);
    boolean isNumber(String s);
    boolean isInRange(int num, int from, int to);
}
