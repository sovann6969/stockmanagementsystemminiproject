package core;

import models.Product;

import java.io.File;
import java.util.List;

public abstract class IProductFile {
   public abstract void write10M();
   public abstract void write(List<Product> products);
   public abstract void write(Product product);
   public abstract List<Product> read(String fileName);
   public abstract void save(List<Product> products);
   public abstract void backup();
   public abstract void restore();

}
