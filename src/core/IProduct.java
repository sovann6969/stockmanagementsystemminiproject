package core;

import models.Product;

import java.util.List;

public interface IProduct {
    List<Product> get();
    Product get(int id);
    Product insert(Product product);
    Product delete(int id);

    List<Product> search(int name);
    List<Product> gotoPage(int page);
    List<Product> first();
    List<Product> last();
    List<Product> previous();
    List<Product> next();

}
