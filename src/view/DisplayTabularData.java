package view;

import core.IDisplayTabularData;
import models.Product;

import java.util.List;

public class DisplayTabularData implements IDisplayTabularData {
    @Override
    public void display(List<Product> products) {

    }

    @Override
    public void display(Product product) {

    }

    @Override
    public void setDisplayRowNumber(int n) {

    }

    @Override
    public void first() {

    }

    @Override
    public void last() {

    }

    @Override
    public void previous() {

    }

    @Override
    public void next() {

    }
}
