package util;

public interface Resource {
    // Capacity and loadFactor

    String WELCOME      = "Welcome to";
    String APP_NAME     = "Stock Management";
    String LOADING      = "Please Wait Loading";
    String CURRENT_TIME = "Current Time Loading: ";

    // Menu
    String DISPLAY  = "*)Display";
    String WRITE    = "W)rite";
    String READ     = "R)ead";
    String UPDATE   = "U)pdate";
    String DELETE   = "D)elete";
    String FIRST    = "F)irst";
    String PREVIOUS = "P)revious";
    String NEXT     = "N)ext";
    String LAST     = "L)ast";
    String SEARCH   = "S)earch";
    String GOTO     = "G)oto";
    String SET_ROW  = "Se)t Row";
    String BACK_UP  = "B)ack Up";
    String RESTORE  = "Re)store";
    String HELP     = "H)elp";
    String EXIT     = "E)xit";


    // Other
    String COMMAND  = "Command --> ";
    String TOTAL_RECORD = "Total records: ";
    int AMOUNT  = 10_000_000;


}
