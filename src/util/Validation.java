package util;

import core.IValidation;

import java.util.regex.Pattern;

public class Validation implements IValidation {
    /**
     * To validate selected menu
     *
     * @author vinsovann
     * @version 1.0
     * @since 2014-03-31
     */
    @Override
    public boolean isShortcut(String s) {
        String pattern_del = "[d][#][a-zA-Z].*[_]\\d.*[_]\\d.*";
        String pattern_write = "[w][#][a-zA-Z].*[_]\\d.*[_]\\d.*";
        String pattern_update = "[u][#][a-zA-Z].*[_]\\d.*[_]\\d.*";
        String pattern_read = "[r][#][a-zA-Z].*[_]\\d.*[_]\\d.*";

        if (Pattern.matches(pattern_del, s))
            return true;
        else if (Pattern.matches(pattern_read, s))
            return true;
        else if (Pattern.matches(pattern_update, s))
            return true;
        else if (Pattern.matches(pattern_write, s))
            return true;
        else return false;

    }

    /**
     * To validate weather it's a number
     *
     * @autor vinsovann
     * @version 1.0
     * @since 2014-03-31
     */
    @Override
    public boolean isNumber(String s) {
        try {
            Integer.parseInt(s);
            if (Integer.parseInt(s) < 0)
                return false;
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * Check Weather it's in range
     *
     * @author vinsovann
     * @version 1.0
     * @since 2014-03-31
     */
    @Override
    public boolean isInRange(int num, int from, int to) {

        if (num < to && num > from)
            return true;

        return false;
    }
}
